from django.urls import path
from accounts.views import login_user, logout_user, sign_up


urlpatterns = [
    path("login/", login_user, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", sign_up, name="signup"),
]
